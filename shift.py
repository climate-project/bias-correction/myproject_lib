import numpy as np

def shift_lon(lon):
    if len(np.where(lon >= 180)[0]):
        shift_index = np.where(lon >= 180)[0][0]
        lon = np.roll(lon, shift_index)
        lon[:shift_index] -= 360
        return lon, shift_index
    else:
        return lon, None

def shift_data(data, shift_index):
    if shift_index:
        data = np.roll(data, shift_index, axis=1)
        return data
    else:
        return data
