import numpy as np


def upscaling(data, lons, lats, scale_factor=4):
    """
    input:
        data: numpy array of spatial data 
            shape = (row, col)
        old_gridceter
    output: 
        new_data: numpy array of rescaling data grid_resolution
            shape = (old_row/factor, old_col/factor)
        new_gridcenter: new lon and lat grid center array
    """

    # output data shape
    new_shape = np.array(data.shape) // scale_factor
    # ending index for drop row and col of original data
    end_index = new_shape * scale_factor

    # drop data and lat, lon
    dropped_data = data[:end_index[0], :end_index[1]]
    new_lons = lons[:end_index[1]]
    new_lats = lats[:end_index[0]]
    # original data shape
    dropped_shape = np.array(dropped_data.shape)

    new_lons = np.linspace(np.min(new_lons), np.max(new_lons), new_shape[1])
    new_lats = np.linspace(np.min(new_lats), np.max(new_lats), new_shape[0])

    # reshape (new_lat, old_lat//new_lat, new_lon, old_lon//new_lon)
    new_data = dropped_data.reshape(
        new_shape[0], dropped_shape[0] // new_shape[0], new_shape[1],
        dropped_shape[1] // new_shape[1]).mean(axis=3).mean(1)

    return new_data, new_lons, new_lats